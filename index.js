/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.
*/

	function totalOf(add1, add2){
		let total = added(add1, add2);
		console.log("Displayed sum of " +add1+ " and " +add2);
		console.log(total)
	}

	function added(add1, add2){
		return add1 + add2;
	}

	totalOf(5, 15);

/*
		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function
*/
	function subtracter(num1, num2){
		let difference = num1 - num2;
		console.log("Displayed difference of " +num1+ " and " +num2);
		console.log(difference);
	}

	subtracter(20, 5);

/*
	2.  Create a function which will be able to multiply two numbers.
		-Numbers must be provided as arguments.
		-Return the result of the multiplication.

	 	Create a global variable called outside of the function called product.
		-This product variable should be able to receive and store the result of multiplication function.

		Log the value of product variable in the console.
*/

	function multiplier(num1, num2){
		let prod = num1*num2;
		return prod;
	}

	let product = multiplier(50, 10);
	console.log("The product of 50 and 10:");
	console.log(product);

/*
		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of quotient variable in the console.

*/
	function divider(num1, num2){
		let quot = num1/num2;
		return quot;
	}

	let quotient= divider(50, 10);
	console.log("The quotient of 50 and 10:");
	console.log(quotient);


/*
	3. 	Create a function which will be able to get total area of a circle from a provided radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.
*/

	function totalArea(radius){
		let pi = 3.14159265358979323846;
		return pi*(radius*radius);
	}

	let circleArea = totalArea(15);
	console.log("The result of getting the area of a circle with 15 radius:");
	console.log(circleArea);

/*
	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
*/
	function totAve(a, b, c, d){
		let average = (a+b+c+d)/4;
		console.log("The average of " +a+ ", " +b+ ", " +c+ ", " +d+ ":");
		return average;
	}

	let averageVar = totAve(20, 40, 60, 80);
	console.log(averageVar);


/*
	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/
	function gradeCalc(score, totalScore){
		let percent1 = score/totalScore;
		let percent2 = percent1*100;
		let isPassed = percent2 > 75;
		return isPassed;
	}

	let isPassingScore = gradeCalc(38, 50);
	console.log("Is 38/50 a passing score?")
	console.log(isPassingScore);